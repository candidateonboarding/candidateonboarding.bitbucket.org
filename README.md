# Candidate Onboarding


Protótipo para o dashboard do time. A ideia do projeto é centralizar
todas as informações e links relevantes do contexto.

- Monitoramento
- Projetos
- Documentações
- Burndown/Jira

[http://candidate-onboarding.gitlab.devel](http://candidate-onboarding.gitlab.devel)


## Start up
Para iniciar a aplicação local use o comando:

```bash
    npm start
```

## Protótipo 
**Atenção**: esse projeto é apenas um protótipo para testar a ideia do 
Dashboard do time.