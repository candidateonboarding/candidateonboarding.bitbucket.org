/**
 * TODO: Refatorar prototipo para diminuir duplicação de código JS e HTML da implementação da Lib.
 */
(function (window, async, moment) {
  /**
   * @namespace
   */
  const ElasticLegacy = window.ElasticLegacy || {};

  /**
   * @memberOf ElasticLegacy
   * @param {{index:string, days:number, filter:object, success:function, error:function}} config
   * @returns {ElasticLegacy.Fetch}
   * @constructor
   */
  ElasticLegacy.Fetch = function (config) {
    /**
     * @type {ElasticLegacy.Fetch}
     */
    let self = this;

    /**
     * Fetch URL logs
     * @type {Array}
     * @private
     */
    this._logs = [];

    /**
     * Chart labels
     * @type {Array}
     * @private
     */
    this._labels = [];

    /**
     * Fetch ElasticLegacy config
     * @private
     */
    this._config = {
      data: {},
      days: 2,
      month: false,
      server: "http://telemetry.servers:9200",
      path: "/{index}-{date}/_search",
      queryString: "?search_type=count",
      success: function () {},
      error: function (err) {}
    };

    /**
     * @private
     * @param {string} index
     * @param {string} date format: YYYY.MM.DD
     * @returns {string}
     */
    this._buildUrl = function (index, date) {
      return this._config.server +
        (this._config.path
          .replace("{index}", index)
          .replace("{date}", date)) +
        this._config.queryString
    };

    if (config !== undefined) {
      for (let property in config) {
        this._config[property] = config[property];
      }
    }

    for (let i = 0, t = self._config.days; i < t; i++) {
      let date = moment().subtract(i, 'days');
      let fetch = function (cb) {
        let dateFormatted = date.format("YYYY.MM.DD");

        if (self._config.month) {
          let from = date.utcOffset(0).set({hour:0,minute:0,second:0,millisecond:0}).valueOf();
          let to = date.utcOffset(0).set({hour:23,minute:59,second:59,millisecond:999}).valueOf();
          dateFormatted = date.format("YYYY.MM");

          self._config.filter.query.filtered.filter.bool.must[0].range =  {
            "@timestamp": {
              "from": from,
              "to": to,
            }
          };
        }

        $.ajax({
          url: self._buildUrl(self._config.index, dateFormatted),
          type: "POST",
          dataType: "json",
          data: JSON.stringify(self._config.filter),
          cache: false,
          success: function (log) {
            cb(null, log);
          },
          error: function (err) {
            cb(err);
          }
        });
      };

      self._labels.push(date.format("DD/MM"));
      self._logs.push(fetch);
    }

    async.series(self._logs, function (err, results) {
      if (err !== null) {
        return self._config.error(err);
      }
      return self._config.success(results.reverse(), self._labels.reverse());
    });
  };

  window.ElasticLegacy = ElasticLegacy;
})(window, window.async, window.moment);
