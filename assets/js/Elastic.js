/**
 * TODO: Refatorar prototipo para diminuir duplicação de código JS e HTML da implementação da Lib.
 */
(function (window, async, $) {
  /**
   * @namespace
   */
  const Elastic = window.Elastic || {};

  /**
   * @memberOf Elastic
   * @param {{index:string, days:number, filter:object, success:function, error:function}} config
   * @returns {Elastic.Fetch}
   * @constructor
   */
  Elastic.Fetch = function (config) {
    /**
     * @type {Elastic.Fetch}
     */
    let self = this;

    /**
     * Fetch URL logs
     * @type {Array}
     * @private
     */
    this._logs = [];

    /**
     * Chart labels
     * @type {Array}
     * @private
     */
    this._labels = [];

    /**
     * Fetch Elastic config
     * @private
     */
    this._config = {
      data: {},
      index: null,
      server: "http://telemetry.servers:5601",
      path: "/elasticsearch/_msearch",
      queryString: "?timeout=0&ignore_unavailable=true&preference=1508161435888",
      success: function () {},
      error: function (err) {}
    };

    /**
     * @private
     * @param {string=undefined} index
     * @param {string=undefined} date format: YYYY.MM.DD
     * @returns {string}
     */
    this._buildUrl = function (index, date) {
      return this._config.server +
        (this._config.path
          .replace("{index}", index)
          .replace("{date}", date)) +
        this._config.queryString
    };

    if (config !== undefined) {
      for (let property in config) {
        this._config[property] = config[property];
      }
    }

    $.ajax({
      url: self._buildUrl(),
      type: "POST",
      dataType: "json",
      data: JSON.stringify(self._config.index) + "\n" +JSON.stringify(self._config.filter),
      cache: false,
      success: function (log) {
        self._config.success(null, log);
      },
      error: function (err) {
        self._config.error(err);
      }
    });
  };

  window.Elastic = Elastic;
})(window, window.async, window.jQuery);
