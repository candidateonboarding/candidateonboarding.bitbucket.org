/**
 * TODO: Refatorar prototipo para diminuir duplicação de código JS e HTML da implementação da Lib.
 */
(function (window, async, moment) {
  /**
   * @namespace
   */
  const Kibana = window.Kibana || {};

  /**
   * @memberOf Kibana
   * @param {{index:string, days:number, filter:object, success:function, error:function}} config
   * @returns {Kibana.Fetch}
   * @constructor
   */
  Kibana.Fetch = function (config) {
    /**
     * @type {Kibana.Fetch}
     */
    let self = this;

    /**
     * Fetch URL logs
     * @type {Array}
     * @private
     */
    this._logs = [];

    /**
     * Chart labels
     * @type {Array}
     * @private
     */
    this._labels = [];

    /**
     * Fetch kibana config
     * @private
     */
    this._config = {
      data: {},
      days: 2,
      server: "http://telemetry.servers:9200",
      path: "/{index}-{date}/_search",
      queryString: "?search_type=count",
      success: function () {},
      error: function (err) {}
    };

    /**
     * @private
     * @param {string} index
     * @param {string} date format: YYYY.MM.DD
     * @returns {string}
     */
    this._buildUrl = function (index, date) {
      return this._config.server +
        (this._config.path
          .replace("{index}", index)
          .replace("{date}", date)) +
        this._config.queryString
    };

    if (config !== undefined) {
      for (let property in config) {
        this._config[property] = config[property];
      }
    }

    for (let i = 0, t = self._config.days; i < t; i++) {
      let date = moment().subtract(i, 'days');
      let fetch = function (cb) {
        $.ajax({
          url: self._buildUrl(self._config.index, date.format("YYYY.MM.DD")),
          type: "POST",
          dataType: "json",
          data: JSON.stringify(self._config.filter),
          cache: false,
          success: function (log) {
            cb(null, log);
          },
          error: function (err) {
            cb(err);
          }
        });
      };

      self._labels.push(date.format("DD/MM"));
      self._logs.push(fetch);
    }

    async.series(self._logs, function (err, results) {
      if (err !== null) {
        return self._config.error(err);
      }
      return self._config.success(results.reverse(), self._labels.reverse());
    });
  };

  window.Kibana = Kibana;
})(window, window.async, window.moment);
