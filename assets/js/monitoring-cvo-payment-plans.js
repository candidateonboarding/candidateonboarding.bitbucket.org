(function (window, Kibana, Chart, $) {
  const filter = {
    "query": {
      "filtered": {
        "query": {
          "bool": {
            "should": [
              {
                "query_string": {
                  "query": "*"
                }
              }
            ]
          }
        },
        "filter": {
          "bool": {
            "must": [
              // {
              //   "range": {
              //     "@timestamp": {
              //       "from": 1505685498799,
              //       "to":   1505765703628
              //     }
              //   }
              // },
              {
                "fquery": {
                  "query": {
                    "query_string": {
                      "query": "appname:(\"microservice-cvo-payment-plans\")"
                    }
                  },
                  "_cache": true
                }
              },
              {
                "fquery": {
                  "query": {
                    "query_string": {
                      "query": "status:(\"412\")"
                    }
                  },
                  "_cache": true
                }
              }
            ],
            "must_not": [
              {
                "terms": {
                  "urlpath": [
                    "/"
                  ]
                }
              },
              {
                "terms": {
                  "useragent": [
                    "curl/7.47.0"
                  ]
                }
              },
              {
                "terms": {
                  "useragent": [
                    "check_http/v2.1.1 (monitoring-plugins 2.1.1)"
                  ]
                }
              },
              {
                "fquery": {
                  "query": {
                    "query_string": {
                      "query": "vhost:(\"testing.seguro.catho.com.br\")"
                    }
                  },
                  "_cache": true
                }
              }
            ]
          }
        }
      }
    },
    "highlight": {
      "fields": {
        "status": {}
      },
      "fragment_size": 2147483647,
      "pre_tags": [
        "@start-highlight@"
      ],
      "post_tags": [
        "@end-highlight@"
      ]
    },
    "size": 50,
    "sort": [
      {
        "status": {
          "order": "desc",
          "ignore_unmapped": true
        }
      },
      {
        "@timestamp": {
          "order": "desc",
          "ignore_unmapped": true
        }
      }
    ]
  };
  const elmToday = $("[data-cvo-pp-errors]");
  const elmTotal = $("[data-cvo-pp-errors-total]");
  const elmChart = $("#chart-cvo-pp-412");
  const createChart = function (labels, data) {
    new Chart(elmChart, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [{
          backgroundColor: "red",
          borderWidth: 0,
          data: data
        }]
      },
      options: {
        scales: {
          xAxes: [{
            barPercentage: 0.3
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        legend: {
          display: false
        }
      }
    });
  };

  new ElasticLegacy.Fetch({
    index: "nginx",
    filter: filter,
    success: function (results, labels) {
      let total = 0;
      let lastDay = 0;
      let totalPerDay = [];

      results.forEach(function (log) {
        if (log !== undefined && log.hits !== undefined) {
          lastDay = log.hits.total;
          total += lastDay;
          totalPerDay.push(lastDay);
        }
      });

      elmToday.html(lastDay);
      elmTotal.html(total);

      createChart(labels, totalPerDay);
    },
    error: function(err) {
      console.log(err);
    }
  });
})(window, window.Kibana, window.Chart, jQuery);

