(function (window, Kibana, Chart, $) {
  const index = {
    "index": "lead-loader-logger-catho-*",
    "ignore_unavailable": true
  };
  const filter = {
    "size": "2000",
      "sort": [{
      "@timestamp": {
        "order": "desc",
        "unmapped_type": "boolean"
      }
    }],
      "query": {
      "filtered": {
        "query": {
          "query_string": {
            "analyze_wildcard": true,
              "query": "*"
          }
        },
        "filter": {
          "bool": {
            "must": [{
              "query": {
                "match": {
                  "message": {
                    "query": "Erro ao gravar ININ: Timeout: Request failed to complete in 30000ms",
                    "type": "phrase"
                  }
                }
              }
            }, {
              "range": {
                "timestamp": {
                  "gte": 1508085661694,
                  "lte": 1508172061695
                }
              }
            }],
              "must_not": []
          }
        }
      }
    },
    "highlight": {
      "pre_tags": ["@kibana-highlighted-field@"],
        "post_tags": ["@/kibana-highlighted-field@"],
        "fields": {
        "*": {}
      },
      "fragment_size": 2147483647
    },
    "aggs": {
      "2": {
        "date_histogram": {
          "field": "timestamp",
            "interval": "30m",
            "pre_zone": "-02:00",
            "pre_zone_adjust_large_interval": true,
            "min_doc_count": 0,
            "extended_bounds": {
            "min": 1508085661694,
              "max": 1508172061694
          }
        }
      }
    },
    "fields": ["*", "_source"],
      "script_fields": {},
    "fielddata_fields": ["timestamp", "@timestamp"]
  };
  const elmToday = $("[data-discador-errors]");
  const elmTotal = $("[data-discador-errors-total]");
  const elmChart = $("#chart-discador-errors");
  const createChart = function (labels, data) {
    new Chart(elmChart, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [{
          backgroundColor: "red",
          borderWidth: 0,
          data: data
        }]
      },
      options: {
        scales: {
          xAxes: [{
            barPercentage: 0.3
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        legend: {
          display: false
        }
      }
    });
  };

   new Elastic.Fetch({
    index: index,
    filter: filter,
    success: function (results, labels) {
      let total = 0;
      let lastDay = 0;
      let totalPerDay = [];

      elmToday.html(lastDay);
      elmTotal.html(total);

      createChart(labels, totalPerDay);
    }
  });
})(window, window.Kibana, window.Chart, jQuery);
