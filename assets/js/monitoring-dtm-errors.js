(function (window, Kibana, Chart, $, moment) {
  const filter = {
    "query": {
      "filtered": {
        "query": {
          "bool": {
            "should": [{
              "query_string": {
                "query": "*"
              }
            }]
          }
        },
        "filter": {
          "bool": {
            "must": [{
              "range": {
                "@timestamp": {
                  "from": moment().subtract(2, 'days').valueOf(),
                  "to": moment().valueOf()
                }
              }
            }, {
              "fquery": {
                "query": {
                  "query_string": {
                    "query": "url:(*restful\\/dtm*)"
                  }
                },
                "_cache": true
              }
            }]
          }
        }
      }
    },
    "highlight": {
      "fields": {},
      "fragment_size": 2147483647,
      "pre_tags": ["@start-highlight@"],
      "post_tags": ["@end-highlight@"]
    },
    "size": 125,
    "sort": [{
      "@timestamp": {
        "order": "desc",
        "ignore_unmapped": true
      }
    }, {
      "@timestamp": {
        "order": "desc",
        "ignore_unmapped": true
      }
    }]
  };

  const elmToday = $("[data-dtm-errors]");
  const elmTotal = $("[data-dtm-errors-total]");
  const elmChart = $("#chart-dtm-errors");
  const createChart = function (labels, data) {
    new Chart(elmChart, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [{
          backgroundColor: "red",
          borderWidth: 0,
          data: data
        }]
      },
      options: {
        scales: {
          xAxes: [{
            barPercentage: 0.3
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        legend: {
          display: false
        }
      }
    });
  };

  new ElasticLegacy.Fetch({
    index: "erros",
    month: true,
    days: 3,
    filter: filter,
    success: function (results, labels) {
      let total = 0;
      let lastDay = 0;
      let totalPerDay = [];

      results.forEach(function (log) {
        if (log !== undefined && log.hits !== undefined) {
          lastDay = log.hits.total;
          total += lastDay;
          totalPerDay.push(lastDay);
        }
      });

      elmToday.html(lastDay);
      elmTotal.html(total);

      createChart(labels, totalPerDay);
    }
  });
})(window, window.Kibana, window.Chart, jQuery, moment);
