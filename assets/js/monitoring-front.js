(function (window, document, $) {
  const elmErrors  = $("[data-front-errors]");
  const elmUsers = $("[data-front-users]");
  const elmIssues = $("[data-front-issues]");

  $(document).ready(function() {
    $.ajax({
      url: 'https://candidate-onboarding.herokuapp.com/sentry/issues',
      type: 'GET',
      dataType: 'json',
      cache: false,
      success: function (response) {
        elmErrors.text(response.total);
        elmUsers.text(response.events);
        response.ranking.forEach(function(issue) {
          elmIssues.append($('<p></p>').html(
            $('<a></a>')
              .attr('target', '_blank')
              .attr('href', issue.permalink)
              .text('['+issue.userCount+'] ' + issue.title)
          ));
        });
      },
      error: function (err) {
      }
    });
  });

})(window, document, window.jQuery);
