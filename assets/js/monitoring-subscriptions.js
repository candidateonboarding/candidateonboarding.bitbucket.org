(function (window, Kibana, Chart, $) {
  const filter = {
    "query": {
      "filtered": {
        "query": {
          "bool": {
            "should": [{
              "query_string": {
                "query": "*"
              }
            }, {
              "query_string": {
                "query": "status:>=500"
              }
            }]
          }
        },
        "filter": {
          "bool": {
            "must": [{
              "fquery": {
                "query": {
                  "query_string": {
                    "query": "appname:(\"microservice-cvo-subscriptions\")"
                  }
                },
                "_cache": true
              }
            }, {
              "fquery": {
                "query": {
                  "query_string": {
                    "query": "method:(\"POST\")"
                  }
                },
                "_cache": true
              }
            }, {
              "fquery": {
                "query": {
                  "query_string": {
                    "query": "status:(\"201\")"
                  }
                },
                "_cache": true
              }
            }],
            "must_not": [{
              "terms": {
                "urlpath": ["/"]
              }
            }, {
              "terms": {
                "useragent": ["curl/7.47.0"]
              }
            }, {
              "terms": {
                "useragent": ["check_http/v2.1.1 (monitoring-plugins 2.1.1)"]
              }
            }]
          }
        }
      }
    },
    "highlight": {
      "fields": {
        "status": {}
      },
      "fragment_size": 2147483647,
      "pre_tags": ["@start-highlight@"],
      "post_tags": ["@end-highlight@"]
    },
    "size": 50,
    "sort": [{
      "status": {
        "order": "asc",
        "ignore_unmapped": true
      }
    }, {
      "@timestamp": {
        "order": "asc",
        "ignore_unmapped": true
      }
    }]
  };
  const elmToday = $("[data-subscriptions]");
  const elmTotal = $("[data-subscriptions-total]");
  const elmChart = $("#chart-subscriptions");
  const createChart = function (labels, data) {
    new Chart(elmChart, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [{
          backgroundColor: "#202020",
          borderWidth: 0,
          data: data
        }]
      },
      options: {
        scales: {
          xAxes: [{
            barPercentage: 0.3
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        legend: {
          display: false
        }
      }
    });
  };

   new ElasticLegacy.Fetch({
    index: "nginx",
    filter: filter,
    success: function (results, labels) {
      let total = 0;
      let lastDay = 0;
      let totalPerDay = [];

      results.forEach(function (log) {
        if (log !== undefined && log.hits !== undefined) {
          lastDay = log.hits.total;
          total += lastDay;
          totalPerDay.push(lastDay);
        }
      });

      elmToday.html(lastDay);
      elmTotal.html(total);

      createChart(labels, totalPerDay);
    }
  });
})(window, window.Kibana, window.Chart, jQuery);
