//TODO: Refatorar Codigo de teste
(function (window, document, $) {
    $(document).ready(function () {
        function sidebar() {
            var linkLogin = $("#link-login"),
                userContainer = $("#user-container"),
                userName = $("#user-name"),
                userImage = $("#user-image");

            function login() {
                var login = $("#ipt-login").val(),
                    pass = $("#ipt-pass").val(),
                    modal = $("#login-modal"),
                    error = $("#login-error");

                $.ajax({
                    url: 'http://gitlab.devel/api/v4/session',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        login: login,
                        password: pass
                    },
                    error: function (data, type, message) {
                        if (message === "Unauthorized") {
                            message = "Usu&aacute;rio ou senha inv&aacute;lida";
                        }
                        error.html(message);
                        error.removeClass("hidden");
                    },
                    success: function (data) {
                        //console.log(data);
                        if (typeof data !== "undefined") {
                            $.cookie("token", data.private_token);
                            $.cookie("avatar_url", data.avatar_url);
                            $.cookie("name", data.name);
                            modal.modal('hide');

                            error.addClass("hidden");
                            showUser(data.name, data.avatar_url)
                        }
                    }
                });
            }

            function logout() {
                $.cookie("token", "");
                $.cookie("avatar_url", "");
                $.cookie("name", "");
                hideUser();
            }

            function showUser() {
                userName.text($.cookie("name"));
                userImage.attr('src', $.cookie("avatar_url"));
                userContainer.removeClass("hidden");
                linkLogin.addClass("hidden");
            }

            function hideUser() {
                linkLogin.removeClass("hidden");
                userContainer.addClass("hidden");
            }

            function activeItem() {
                var url = window.location.pathname;

                $("[data-menu-link]").each(function (index, elm) {
                    if (url.indexOf($(elm).attr("data-menu-link")) === 1) {
                        $(elm).addClass("active");
                    }
                });
            }

            function init() {
                activeItem();

                if ($.cookie("token")) {
                    showUser();
                } else {
                    hideUser();
                }

                $("#btn-logout").on('click', logout);
                $("#btn-login").on('click', login);
            }

            init();
        }

        $('#sidebar').load("/includes/sidebar.html", sidebar);
    });
})(window, document, $);
