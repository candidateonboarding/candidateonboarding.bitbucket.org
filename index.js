const https = require('https');

https.request({
  host: 'sentry.io',
  path: '/api/0/projects/catho-cv/P2/issues/',
  method: 'GET',
  headers: {
    'Authorization': 'bearer f9599932324c4c7b9fc3d0c1dd73d896dfd5453b01c64c589852255f51b1d6e4'
  }
}, (response) => {
  let str = '';
  response.on('data', chunk => str += chunk);
  response.on('end', () => console.log(str));
}).end();
